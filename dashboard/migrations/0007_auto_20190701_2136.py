# Generated by Django 2.2.1 on 2019-07-01 09:36

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('dashboard', '0006_auto_20190701_2133'),
    ]

    operations = [
        migrations.AlterField(
            model_name='choreroster',
            name='rotate_every_n_days',
            field=models.IntegerField(default=7),
        ),
        migrations.AlterField(
            model_name='reminder',
            name='active_for_n_days',
            field=models.IntegerField(default=1),
        ),
        migrations.AlterField(
            model_name='reminder',
            name='recur_every_n_days',
            field=models.IntegerField(default=0),
        ),
    ]
