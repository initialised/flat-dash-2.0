# Generated by Django 2.2.1 on 2019-06-30 10:16

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('dashboard', '0003_flattie_household'),
    ]

    operations = [
        migrations.AlterField(
            model_name='chore',
            name='chore_idx',
            field=models.IntegerField(blank=True, null=True),
        ),
        migrations.AlterField(
            model_name='flattie',
            name='chore_idx',
            field=models.IntegerField(blank=True, null=True),
        ),
    ]
