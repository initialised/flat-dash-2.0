from django.shortcuts import render
from django.http import HttpResponse, HttpResponseRedirect
from django.template import loader
from .models import Household, Reminder, ShoppingListItem, Flattie, Chore, ChoreRoster
from .forms import ReminderForm, ShoppingListForm
from django.urls import reverse
from django.contrib.auth.forms import AuthenticationForm
from django.contrib.auth import login
from django.contrib.auth.decorators import login_required
from django.shortcuts import get_object_or_404
from django.views import generic
from django.utils import timezone


@login_required
def roster(request):
    flat = get_users_household(request.user)
    chore_assignments = []

    if ChoreRoster.objects.all().filter(household=flat).exists():
        roster = ChoreRoster.objects.filter(household=flat).first()
        flatties = Flattie.objects.all().filter(household=flat).order_by('chore_idx')
        chores = Chore.objects.all().filter(roster=roster).order_by('chore_idx')

        current_roster_rotation_idx = roster.get_current_roster_rotation_idx()

        for flattie in flatties:
            idx_of_assigned_chore = flattie.chore_idx + current_roster_rotation_idx
            assigned_chore = Chore.objects.all().filter(chore_idx=idx_of_assigned_chore).first()
            chore_assignments.append({'flattie':flattie, 'chore':assigned_chore})

    return render(request, 'dashboard/roster.html', {'chore_assignments': chore_assignments})


@login_required
def manage_roster(request):
    flat = get_users_household(request.user)
    chore_assignments = []

    if ChoreRoster.objects.all().filter(household=flat).exists():
        roster = ChoreRoster.objects.filter(household=flat).first()
        flatties = Flattie.objects.all().filter(household=flat).order_by('chore_idx')
        chores = Chore.objects.all().filter(roster=roster).order_by('chore_idx')

        current_roster_rotation_idx = roster.get_current_roster_rotation_idx()

        for flattie in flatties:
            idx_of_assigned_chore = flattie.chore_idx + current_roster_rotation_idx
            assigned_chore = Chore.objects.all().filter(chore_idx=idx_of_assigned_chore).first()
            chore_assignments.append({'flattie':flattie, 'chore':assigned_chore})

    return render(request, 'dashboard/roster.html', {'chore_assignments': chore_assignments})


@login_required
def shopping(request):

    flat = get_users_household(request.user)

    delete_item_submit_value = 'delete'
    delete_item_id_post_idx = 'delete_idx'
    shopping_list_submit_value = "shopping"
    shopping_list_form = save_new_delete_or_get_form(ShoppingListItem, ShoppingListForm, flat,
                                                     request, shopping_list_submit_value, delete_item_submit_value, delete_item_id_post_idx)
    if shopping_list_form is None:
        return HttpResponseRedirect(reverse('dashboard:shopping'))

    shopping_list = flat.shoppinglistitem_set.all()

    context = {
        'shopping_list': shopping_list,
        'shopping_list_form': shopping_list_form,
        'shopping_list_submit_value': shopping_list_submit_value,
        'delete_item_submit_value': delete_item_submit_value,
        'delete_item_id_post_idx': delete_item_id_post_idx,
    }
    return render(request, 'dashboard/shopping.html', context)


@login_required
def reminder(request):

    flat = get_users_household(request.user)

    delete_item_submit_value = 'delete'
    delete_item_id_post_idx = 'delete_idx'
    # create/submit model forms with instances referencing the household
    reminder_submit_value = "reminder"
    reminder_form = save_new_delete_or_get_form(Reminder, ReminderForm, flat, request,
                                                reminder_submit_value, delete_item_submit_value, delete_item_id_post_idx)
    if reminder_form is None:
        return HttpResponseRedirect(reverse('dashboard:reminders'))

    reminder_list = flat.reminder_set.all()

    active_reminders = [reminder for reminder in reminder_list if reminder.is_active()]

    context = {
        'reminder_list': active_reminders,
        'reminder_form': reminder_form,
        'reminder_submit_value': reminder_submit_value,
        'delete_item_submit_value': delete_item_submit_value,
        'delete_item_id_post_idx': delete_item_id_post_idx,
    }
    return render(request, 'dashboard/reminders.html', context)


def save_new_delete_or_get_form(model_class, model_form_class, flat, request, submit_value,
                                delete_item_submit_value, delete_item_id_post_idx):
    # if user is adding new instance of this type
    if submit_value in request.POST:
        return save_new_model(model_class, model_form_class, flat, request)
    # if user is removing an object from the list
    if delete_item_submit_value in request.POST:
        return delete_an_object(model_class, item_id=request.POST[delete_item_id_post_idx])
    model_form = model_form_class(None)
    return model_form


def delete_an_object(model_class, item_id):
    model_class.objects.filter(id=item_id).delete()
    return None


def save_new_model(model_class, model_form_class, flat, request):
    # populate new instance with FK to this household
    new_instance = model_class(household=flat)
    model_form = model_form_class(request.POST, instance=new_instance)

    if model_form.is_valid():
        new_object = model_form.save(commit=False)
        new_object.added_by = request.user
        new_object.save()
    return None


def get_users_household(user):
    # user_id = user.get_user_id()
    flattie = get_object_or_404(Flattie, user=user)
    household = flattie.household
    return household


def login_view(request):
    if request.method == "POST":
        form = AuthenticationForm(data=request.POST)
        if form.is_valid():
            #login
            user = form.get_user()
            login(request, user)
            return HttpResponseRedirect('dashboard')
    else:
        form = AuthenticationForm()
    return render(request, 'dashboard/login.html', {'login_form': form})
