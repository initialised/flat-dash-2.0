from django.contrib import admin
from django.contrib.auth.admin import UserAdmin as BaseUserAdmin
from django.contrib.auth.models import User

from dashboard.models import Flattie, Household, ShoppingListItem, ChoreRoster, Chore


# Define an inline admin descriptor for Flattie model
# which acts a bit like a singleton
# profile model stores extra info compared to User, add extra fields to the admin page for users
class FlattieInline(admin.StackedInline):
    model = Flattie
    can_delete = False
    verbose_name_plural = 'flatmates'


# Define a new User admin
class UserAdmin(BaseUserAdmin):
    inlines = (FlattieInline,)


# Re-register UserAdmin
admin.site.unregister(User)
admin.site.register(User, UserAdmin)

# Register my models
admin.site.register(Flattie)
admin.site.register(Household)
admin.site.register(ShoppingListItem)
admin.site.register(ChoreRoster)
admin.site.register(Chore)
