from django.db import models
from django.utils import timezone
from django.contrib.auth.models import User
from datetime import date, timedelta

# require user email address to be defined when creating a user
# User._meta.get_field('email').blank = False


class Household(models.Model):
    name = models.CharField(max_length=50)

    def __str__(self):
        return self.name


# members of household extend auth user class
# create profile model to store chore index
class Flattie(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE)
    # used to map the rotation of chores to users in the correct order
    chore_idx = models.IntegerField(blank=True, null=True)
    # household has flatmates
    household = models.ForeignKey(
        Household,
        on_delete=models.CASCADE,
    )

    def __str__(self):
        return self.user.__str__()


class ChoreRoster(models.Model):
    created_on = models.DateField(default=timezone.now)
    start_at = models.DateField(default=timezone.now)
    # rotate_every_n_days = models.DurationField(default=timedelta(days=7))
    rotate_every_n_days = models.IntegerField(default=7)
    # Household has a chore roster
    household = models.ForeignKey(
        Household,
        on_delete=models.CASCADE,
    )

    def get_current_roster_rotation_idx(self):
        """ Rotation index tells us which permutation of the roster we are up to
        e.g. if there are 4 possible chores (indexes 0-3) and it's the first week of the roster
        it's index 0 and if it's the sixth week it is index 1"""
        now = timezone.now().date()
        # if the roster hasn't begun yet
        if self.start_at > now:
            return 0
        days_passed = (now - self.start_at).days
        n_rotations = Chore.objects.filter(roster=self).count()
        rotation_idx = (days_passed % self.rotate_every_n_days) % n_rotations
        return rotation_idx



# Chore object per chore to be done
class Chore(models.Model):
    name = models.CharField(max_length=30)
    # used to map the rotation of chores to users in the correct order
    chore_idx = models.IntegerField(blank=True, null=True)
    # Each chore belongs to a roster
    roster = models.ForeignKey(
        ChoreRoster,
        on_delete=models.CASCADE,
    )

    def __str__(self):
        return self.name


# Reminder for landlord visits, bin days etc
class Reminder(models.Model):
    text = models.CharField(max_length=300)
    start_at = models.DateField(default=timezone.now)
    # active_for_n_days = models.DurationField(default=timedelta(hours=24))
    # recur_every_n_days = models.DurationField(default=timedelta(days=7))
    active_for_n_days = models.IntegerField(default=1)
    recur_every_n_days = models.IntegerField(default=0)
    # Household has many reminders
    household = models.ForeignKey(
        Household,
        on_delete=models.CASCADE,
    )
    # Could be helpful for users to know who is adding things
    added_by = models.ForeignKey(
        User,
        on_delete=models.CASCADE,
    )

    def __str__(self):
        return self.title

    def is_active(self):
        now = timezone.now().date()
        start_date = self.start_at

        if now >= start_date and now <= (start_date+timedelta(days=self.active_for_n_days)):
            return True
        # if we haven't reached the start date or this isn't a recurring reminder
        if now < start_date or self.recur_every_n_days == 0:
            return False

        days_between = int((now - start_date).days)
        n_day_of_cycle = days_between % self.recur_every_n_days

        # if it is one of the reminders active days in the cycle
        if n_day_of_cycle >= self.active_for_n_days:
            return True




# Reminder for flat groceries
class ShoppingListItem(models.Model):
    text = models.CharField(max_length=300)
    # Household has many things on a shopping list
    household = models.ForeignKey(
        Household,
        on_delete=models.CASCADE,
    )
    # Could be helpful for users to know who is adding things
    added_by = models.ForeignKey(
        User,
        on_delete=models.CASCADE,
    )

    def __str__(self):
        return self.text