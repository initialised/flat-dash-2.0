from django.forms import ModelForm  #, inlineformset_factory
# from django.forms.widgets import
from .models import Reminder, ShoppingListItem  #, Household
from django.utils.translation import gettext_lazy as _

# Create input form based on Reminder model
class ReminderForm(ModelForm):
    class Meta:
        model = Reminder
        fields = ['text', 'start_at', 'active_for_n_days', 'recur_every_n_days',]
        labels = {
            'active_for_n_days': _('Active for __ days'),
            'recur_every_n_days': _('Recur after __ days (zero for never)'),
        }
        # widgets = {'time': DateTimeInput}


# Create input form based on Shopping List Item model
class ShoppingListForm(ModelForm):
    class Meta:
        model = ShoppingListItem
        fields = ['text',]
        labels = {
            'text': _(''),
        }