from django.urls import path
from django.views.generic import RedirectView
from . import views

app_name = 'dashboard'

urlpatterns = [
    path('shopping/', views.shopping, name='shopping'),
    path('reminders/', views.reminder, name='reminders'),
    path('roster/', views.roster, name='roster'),
    path('', RedirectView.as_view(url='shopping/', permanent=True)),
]